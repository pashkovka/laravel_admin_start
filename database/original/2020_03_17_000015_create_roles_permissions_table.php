<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesPermissionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'roles_permissions';

    /**
     * Run the migrations.
     * @table roles_permissions
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('roles_id');
            $table->unsignedInteger('permissions_id');
        });

        Schema::table($this->tableName, function (Blueprint $table) {
            $table->index(["roles_id"], 'fk_roles_has_permissions_roles1_idx');
            $table->index(["permissions_id"], 'fk_roles_has_permissions_permissions1_idx');

            $table->foreign('roles_id', 'fk_roles_has_permissions_roles1_idx')
                ->references('id')->on('roles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('permissions_id', 'fk_roles_has_permissions_permissions1_idx')
                ->references('id')->on('permissions')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
