<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRolesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'users_roles';

    /**
     * Run the migrations.
     * @table users_roles
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('users_id');
            $table->unsignedInteger('roles_id');
        });

        Schema::table($this->tableName, function (Blueprint $table){
            $table->index(["users_id"], 'fk_users_has_roles_users1_idx');
            $table->index(["roles_id"], 'fk_users_has_roles_roles1_idx');

            $table->foreign('users_id', 'fk_users_has_roles_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('roles_id', 'fk_users_has_roles_roles1_idx')
                ->references('id')->on('roles')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
