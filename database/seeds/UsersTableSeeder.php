<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $users = [
            [
                'name' => 'Сергей Смирнов',
                'email' => 'pariss8@mail.ru',
                'email_verified_at' => now(),
                'remember_token' => \Illuminate\Support\Str::random(),
                'password' => bcrypt('12345678'),
                'created_at' => now()
            ],
            [
                'name' => $faker->name,
                'email' => $faker->email,
                'email_verified_at' => now(),
                'remember_token' => \Illuminate\Support\Str::random(),
                'password' => bcrypt('12345678'),
                'created_at' => now()
            ],
        ];

        /*for ($i = 1; $i < 20; $i++) {
            $user = [
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'email_verified_at' => now(),
                'remember_token' => \Illuminate\Support\Str::random(),
                'password' => bcrypt('12345678'),
                'gender' => ($i % 3 == 0 ? 'w' : 'm'),
                'birth_date' => $faker->date('Y-m-d'),
                'created_at' => now()
            ];
            $users[] = $user;
        }*/

        DB::table('users')->insert($users);
    }

}
