<?php
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'dashboard_view', 'description' => 'Просматривать главную панель']);

        Permission::create(['name' => 'user_view', 'description' => 'Просматривать пользователей']);
        Permission::create(['name' => 'user_edit', 'description' => 'Редактировать пользователя']);
        Permission::create(['name' => 'user_add', 'description' => 'Добавлять пользователя']);
        Permission::create(['name' => 'user_delete', 'description' => 'Удалять пользователя']);

        Permission::create(['name' => 'permission_view', 'description' => 'Просматривать разрешения']);
        Permission::create(['name' => 'permission_edit', 'description' => 'Редактировать разрешение']);

        Permission::create(['name' => 'role_view', 'description' => 'Просматривать роли']);
        Permission::create(['name' => 'role_edit', 'description' => 'Редактировать роль']);
        Permission::create(['name' => 'role_add', 'description' => 'Добавлять роль']);
        Permission::create(['name' => 'role_delete', 'description' => 'Удалять роль']);

        Permission::create(['name' => 'user-role_view', 'description' => 'Просматривать роли пользователей']);
        Permission::create(['name' => 'user-role_edit', 'description' => 'Синхронизировать пользователей и роли']);
//        Permission::create(['name' => 'user-role_add', 'description' => 'Добавлять роль']);
//        Permission::create(['name' => 'user-role_delete', 'description' => 'Удалять роль']);

        Permission::create(['name' => 'file-mager_*', 'description' => 'Использовать файловый менеджер']);

        // create roles and assign created permissions

        // this can be done as separate statements
//        $role = Role::create(['name' => 'super_admin']);
//        $role->givePermissionTo('edit articles');
//
//        // or may be done by chaining
//        $role = Role::create(['name' => 'moderator'])
//            ->givePermissionTo(['publish articles', 'unpublish articles']);


        $role = Role::create(['name' => 'Администратор', 'description' => 'Можно всё']);
        $role->givePermissionTo(Permission::all());

        Role::create(['name' => 'Кассир', 'description' => 'Просматривает главную'])->givePermissionTo(['dashboard_view']);

        $users = \App\Models\User::all();
        if ($users){
            $admin = $users[0];
            $admin->assignRole('Администратор');
            if (isset($users[1])){
                $moderator = $users[1];
                $moderator->assignRole('Кассир');
            }
        }
    }
}






















