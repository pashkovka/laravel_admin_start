@extends('layouts.app')
@section('content')
    <div class="block_category">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Каталог</h1>
                </div>
                <div class="col-lg-12">
                    @include('parts.category_nav')
                    <div class="cat_container">
                        <div class="row">
                            @foreach($users as $user)
                                @php
                                    if ($user->profile) {$profile = $user->profile;} else{$profile = null;}
                                @endphp
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="public/assets/site/images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="javascript:void(0);" class="location-event">
                                            {{ $profile->city }}, {{ $profile->country }}
                                        </a>
                                        <a href="#" class="name-autor">
                                            {{$user->name}}
                                        </a>
                                        <p class="text-autor">
                                            @if($profile)
                                                {{$profile->description}}
                                            @endif
                                        </p>
                                        <div class="rating-event">
                                            @if($profile)
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">{{$profile->raiting - 1}}</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                                @endif
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                @php
$tours = $user->tours_with_categories;
if ($tours){
    $cats = [];
    foreach ($tours as $tour) {
        foreach ($tour->categories as $category){
          if (!in_array($category->id, $cats)){
              echo "<a href=\"{$category->id}\">{$category->title}</a>";
          }
            $cats[] = $category->id;
        }
    }
}
                                                @endphp
                                                {{--<a href="#">Категория1</a>
                                                <a href="#">Категория2</a>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            {{--<div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Яков Радченко
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Организация тест
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Яков Радченко
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Яков Радченко
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Яков Радченко
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Яков Радченко
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Яков Радченко
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Яков Радченко
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Яков Радченко
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Яков Радченко
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="event-list-autor">
                                    <img src="images/slider_img_autor.jpg" alt="" class="img-fluid img-list-autor">
                                    <div class="block-list-autor">
                                        <a href="#" class="location-event">
                                            Москва, Россия
                                        </a>
                                        <a href="#" class="name-autor">
                                            Яков Радченко
                                        </a>
                                        <p class="text-autor">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum fugit incidunt quasi deserunt, libero placeat.
                                        </p>
                                        <div class="rating-event">
                                            <div class="rating">
														<span class="rating-star-display">
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-solid"></span>
															<span class="rating-star-half"></span>
															<span class="rating-value">4.5</span>
														</span>
                                                <span class="review-count">(32 отзывов)</span>
                                            </div>
                                        </div>
                                        <div class="event-footer">
                                            <div class="event-tags">
                                                <a href="#">Категория1</a>
                                                <a href="#">Категория2</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--}}
                        </div>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
